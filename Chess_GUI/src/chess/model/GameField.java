package chess.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * A class whose sole responsibility is the management of pawns on the chess board. As such it
 * provides the data structure that allows to check and manipulate each entry on the board
 * accordingly.
 */
public class GameField {

  public static final int SIZE = 8;

  private final Pawn[][] field = new Pawn[SIZE][SIZE];

  public GameField() {

  }

  /**
   * Perform a deep copy of gameField variable.
   * @param gameField is the game field that should be copied.
   */
  GameField(GameField gameField) {
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        Cell checkedCell = new Cell(j,i);
        if (gameField.get(checkedCell).isPresent()) {
          Pawn pawn = gameField.get(checkedCell).get();
          set(checkedCell,new Pawn(pawn));
        }
      }
    }
  }

  /**
   * Return an {@link Optional} that may contain a {@link Pawn}, depending on if there is one
   * positioned on the cell.
   *
   * @param cell The cell to be checked.
   * @return An {@link Optional optional} containing the respective pawn in case of success.
   */
  public Optional<Pawn> get(Cell cell) {
    throwErrorWhenOutOfBounds(cell);
    return Optional.ofNullable(field[cell.getColumn()][cell.getRow()]);
  }

  /**
   * Returns all {@link Cell cells} that are currently occupied by a pawn.
   *
   * @return A map with all cells that have a pawn on them.
   */
  public Map<Cell, Player> getCellsOccupiedWithPawns() {
    Map<Cell, Player> map = new HashMap<>();

    for (int column = 0; column < SIZE; column++) {
      for (int row = 0; row < SIZE; row++) {
        if (field[column][row] != null) {
          map.put(new Cell(column, row), field[column][row].getPlayer());
        }
      }
    }

    return map;
  }

  /**
   * Set a pawn on the given cell. Any pawns already on that cell will be overridden.
   *
   * @param cell cell to set pawn on
   * @param newValue new value (pawn) to set on the cell
   * @throws IllegalArgumentException if given cell is out of field bounds
   */
  void set(Cell cell, Pawn newValue) {
    throwErrorWhenOutOfBounds(cell);
    Objects.requireNonNull(newValue);

    field[cell.getColumn()][cell.getRow()] = newValue; // may override an existing pawn
  }

  /**
   * Remove pawn from the given cell. This method only has to work if there is a pawn on the cell.
   *
   * @param cell cell to remove any pawn from
   * @return the pawn that was removed
   * @throws IllegalArgumentException if given cell is out of field bounds
   */
  Pawn remove(Cell cell) {
    throwErrorWhenOutOfBounds(cell);
    int col = cell.getColumn();
    int row = cell.getRow();
    if (field[col][row] == null) {
      throw new IllegalArgumentException("There's no pawn to delete for cell " + cell);
    }

    Pawn removed = field[col][row];
    field[col][row] = null;
    return removed;
  }

  /**
   * Checks a {@link Cell cell} whether it is occupied by a pawn and in case of success, whether it
   * belongs to the respective {@link Player player}.
   *
   * @param player The player to be checked.
   * @param cell The cell that can contain a pawn.
   * @return <code>true</code> if the player has a pawn on the cell, <code>false</code> otherwise.
   */
  boolean isCellOfPlayer(Player player, Cell cell) {
    Optional<Pawn> pawnOpt = get(cell);
    return pawnOpt.isPresent() && pawnOpt.get().getPlayer() == player;
  }

  /**
   * Checks a cell for its bounds and throws an exception in case of failure.
   *
   * @param cell The cell to be checked.
   */
  private void throwErrorWhenOutOfBounds(Cell cell) {
    if (!isWithinBounds(cell)) {
      throw new IllegalArgumentException("Coordinates of cell are out of bounds: " + cell);
    }
  }

  /**
   * Checks a {@link Cell} if its column- and row-value is within the bounds. The valid range is
   * from 0 to 7 for each.
   *
   * @param cell The cell to be checked
   * @return <code>true</code> if within the bounds, <code>false</code> otherwise.
   */
  public boolean isWithinBounds(Cell cell) {
    return cell.getColumn() >= 0
        && cell.getColumn() < SIZE
        && cell.getRow() >= 0
        && cell.getRow() < SIZE;
  }
}
