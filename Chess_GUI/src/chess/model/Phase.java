package chess.model;

/** Provides constants that represent the possible stages of this chess game. */
public enum Phase {
  RUNNING,
  FINISHED,
}
