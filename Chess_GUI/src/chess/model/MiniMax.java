package chess.model;

import java.util.List;
import java.util.Optional;

public class MiniMax {
  private int maxDepth = 3;

  /**
   * This the main method of minimax algorithm. The method called
   * min function and max function recursively.
   * @param stateTree is the node.
   * @return it returns the value of the node.
   * @throws CloneNotSupportedException because a deep copy is executed here.
   */
  public double think(ModelNode stateTree) throws CloneNotSupportedException {
    if (stateTree.getHeight() == maxDepth || isLeaf(stateTree)) {
      return stateTree.getScore();
    }
    if (stateTree.getModel().getState().getCurrentPlayer() == Player.BLACK) {
      return max(stateTree);
    }
    return min(stateTree);
  }

  /**
   * find the min score from children's node.
   * @param stateTree is the node
   * @return it returns the min score from the node's children.
   * @throws CloneNotSupportedException because a deep copy is executed here.
   */
  private double min(ModelNode stateTree) throws CloneNotSupportedException {
    stateTree.getModel().createChildren(stateTree);
    Optional<ModelNode> optNode = Optional.ofNullable(stateTree.getChildren().get(0));
    if (optNode.isEmpty()) {
      return stateTree.getScore();
    }
    double oldScore = Double.POSITIVE_INFINITY;

    List<ModelNode> children = stateTree.getChildren();

    for (ModelNode child : children) {
      //1System.out.println("min, (" + child.getFrom() + "),("+ child.getTo() + ")
      // , gameScore = " + child.getModel().getGameScore() +
      //        ", stateScore = " + child.getScore() + ": ");
      double childScore = think(child);
      //1System.out.println("mint, (" + child.getFrom() + "),(" + child.getTo() + ")
      // , gameScore = " + child.getModel().getGameScore() +
      //        ", stateScore = " + child.getScore() + ", childScore = " + childScore);
      //1System.out.println(" ");
      if (childScore < oldScore) {
        oldScore = childScore;
        stateTree.setFavouriteChild(child);
      }
    }
    double score = stateTree.getScore() + stateTree.getFavouriteChild().getScore();
    stateTree.setScore(score);
    //2System.out.println("So move to " + stateTree.getFavouriteChild().getTo());
    return stateTree.getScore();
  }

  /**
   * find the max score from children's node.
   * @param stateTree is the node
   * @return it returns the min score from the node's children.
   * @throws CloneNotSupportedException because a deep copy is executed here.
   */
  private double max(ModelNode stateTree) throws CloneNotSupportedException {
    stateTree.getModel().createChildren(stateTree);

    //check whether a child exist
    Optional<ModelNode> optNode = Optional.ofNullable(stateTree.getChildren().get(0));
    if (optNode.isEmpty()) {
      return stateTree.getScore();
    }

    double oldScore = Double.NEGATIVE_INFINITY;

    List<ModelNode> children = stateTree.getChildren();

    for (ModelNode child : children) {
      //1System.out.println("max, (" + child.getFrom() + "),("+ child.getTo() + ")
      // , gameScore = " + child.getModel().getGameScore() +
      //        ", stateScore = " + child.getScore() + ": ");
      double childScore = think(child);
      //1System.out.println("maxt, (" + child.getFrom() + "),(" + child.getTo() + ")
      // , gameScore = " + child.getModel().getGameScore() +
      //        ", stateScore = " + child.getScore() + ", childScore = " + childScore);
      //1System.out.println(" ");

      if (childScore > oldScore) {
        oldScore = childScore;
        stateTree.setFavouriteChild(child);
      }
    }
    double score = stateTree.getScore() + stateTree.getFavouriteChild().getScore();
    stateTree.setScore(score);
    //2System.out.println("So move to " + stateTree.getFavouriteChild().getTo());
    //2System.out.println("--------------------------------------------");
    return stateTree.getScore();
  }

  /**
   * checked whether the given node is leaf or not.
   * @param stateTree is the node that must be checked.
   * @return if it's a leaf, then it returns true, otherwise false.
   */
  private boolean isLeaf(ModelNode stateTree) {
    return stateTree.getChildren() == null;
  }

}