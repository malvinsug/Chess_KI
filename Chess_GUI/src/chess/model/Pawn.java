package chess.model;

/** A class representing a single pawn of the chess board including its corresponding color. */
public class Pawn {

  private final Player player;

  /**
   * Create a new <code>Pawn</code>-object that is owned by the specified player.
   *
   * @param player The owner of the pawn.
   */
  Pawn(Player player) {
    this.player = player;
  }

  /**
   * Perform a deep copy of pawn variable.
   * @param pawn is the pawn that should be copied.
   */
  Pawn(Pawn pawn) {
    this.player = pawn.getPlayer();
  }

  /**
   * Return the {@link Player} that is the owner of this <code>Pawn</code>.
   *
   * @return The owning player.
   */
  public Player getPlayer() {
    return player;
  }
}
