package chess.model;

import static java.util.Objects.requireNonNull;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Implementation of a chess class that handles the logic for a chess game that consists of pawns
 * only. It is the entry-class to the internal structure of the game-model and provides all methods
 * necessary for an user-interface to playing a game successfully.
 */
public class Chess implements Model {

  private static final int DIRECTION_UPWARD = 1;
  private static final int DIRECTION_DOWNWARD = -1;

  private final PropertyChangeSupport support;
  private GameState state;
  private double gameScore;
  private boolean artIntelligent;
  private boolean think;

  /**
   * Initialize a new Chess-Game in which everything is set up in its initial position. The game is
   * ready to be played immediately after.
   */
  public Chess() {
    support = new PropertyChangeSupport(this);
    newGame();
  }

  /**
   * perform a deep copy from model variable.
   * @param model is the chess model that must be copied.
   */
  public Chess(Model model) {
    this.state = new GameState(model.getState());
    this.artIntelligent = model.getArtIntelligent();
    this.support = new PropertyChangeSupport(this);
    this.gameScore = model.getGameScore();
    this.think = true;
  }

  /**
   * Put all pawns in their initial state on the gaming board. This is done for both {@link Player
   * players}, where {@link Player#BLACK} starts in the top row and {@link Player#WHITE} in the
   * bottom row.
   */
  private void setInitialPawns() {
    GameField field = state.getField();
    int blackRow = getBaseRowIndexOfPlayer(Player.BLACK);
    int whiteRow = getBaseRowIndexOfPlayer(Player.WHITE);

    for (int column = 0; column < GameField.SIZE; column++) {
      // The cells of both players start in the same row respectively. Only the column values
      // change in each iteration.
      Cell blackCell = new Cell(column, blackRow);
      field.set(blackCell, new Pawn(Player.BLACK));

      Cell whiteCell = new Cell(column, whiteRow);
      field.set(whiteCell, new Pawn(Player.WHITE));
    }
  }

  @Override
  public void addPropertyChangeListener(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    support.addPropertyChangeListener(pcl);
  }

  @Override
  public void removePropertyChangeListener(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    support.removePropertyChangeListener(pcl);
  }

  /**
   * Invokes the firing of an event, such that any attached observer (i.e., {@link
   * PropertyChangeListener}) is notified that a change happened to this model.
   */
  private void notifyListeners() {
    support.firePropertyChange(STATE_CHANGED, null, this);
  }

  /**
   * Returns the row-index in which the pawns of the respective player start.
   *
   * @param player The player whose starting row is to be determined.
   * @return the row-index, ranging from 0 to 7.
   */
  private int getBaseRowIndexOfPlayer(Player player) {
    switch (player) {
      case BLACK:
        return GameField.SIZE - 1;
      case WHITE:
        return 0;
      default:
        throw new AssertionError("Unhandled player: " + player);
    }
  }

  @Override
  public GameState getState() {
    return state;
  }

  @Override
  public void newGame() {
    state = new GameState();
    gameScore = ScoringState.initialScore();
    this.think = false;
    setInitialPawns();
    notifyListeners();
  }

  @Override
  public boolean move(Cell from, Cell to) throws CloneNotSupportedException {
    if (state.getCurrentPhase() != Phase.RUNNING) {
      return false;
    }

    GameField field = state.getField();
    Player currentPlayer = state.getCurrentPlayer();
    // Source cell must be from the current active player.
    if (!field.isWithinBounds(from)
            || !field.isWithinBounds(to)
            || !field.isCellOfPlayer(currentPlayer, from)) {
      return false;
    }

    // get all possible moves for the specific pawn
    Set<Cell> possibleMoves = getPossibleMovesForPawn(currentPlayer, from);
    if (!possibleMoves.contains(to)) {
      return false;
    }

    Pawn movedPawn = state.getField().remove(from);
    state.getField().set(to, movedPawn);

    Player nextPlayer = Player.getOpponentOf(currentPlayer);
    if (!checkForWinningCondition() && canExecuteMove(nextPlayer)) {
      state.setCurrentPlayer(nextPlayer);
    }
    this.gameScore = ScoringState.updateAll(this,from,to);
    //if (!this.think) {
    //  System.out.println(this.gameScore);
    //}


    if (this.artIntelligent && getState().getCurrentPlayer() == Player.BLACK && !this.think) {
      Model newModel = new Chess(this);
      ModelNode stateTree = new ModelNode(newModel,from,to);
      MiniMax computer = new MiniMax();
      computer.think(stateTree);
      move(stateTree.getFavouriteChild().getFrom(),stateTree.getFavouriteChild().getTo());
      //System.out.println(stateTree.getFavouriteChild().getModel().getGameScore());
    }

    notifyListeners();
    return true;
  }

  /**
   * Checks whether a winning condition is fulfilled. In that case, the {@link Phase phase} and the
   * {@link Player winning player} are set appropriately.
   *
   * @return <code>true</code> if the game is over, <code>false</code> otherwise
   */
  private boolean checkForWinningCondition() {
    if (state.getCurrentPhase() != Phase.RUNNING) {
      return true;
    }

    // check if player black has reached the base line of player white
    Set<Cell> blackPawns = state.getAllCellsOfPlayer(Player.BLACK);
    for (Cell cell : blackPawns) {
      if (cell.getRow() == getBaseRowIndexOfPlayer(Player.WHITE)) {
        state.setCurrentPhase(Phase.FINISHED);
        state.setWinner(Optional.of(Player.BLACK));
        return true;
      }
    }

    // check if player white has reached the base line of player black
    Set<Cell> whitePawns = state.getAllCellsOfPlayer(Player.WHITE);
    for (Cell cell : whitePawns) {
      if (cell.getRow() == getBaseRowIndexOfPlayer(Player.BLACK)) {
        state.setCurrentPhase(Phase.FINISHED);
        state.setWinner(Optional.of(Player.WHITE));
        return true;
      }
    }

    // check whether player black is still able to make a move
    boolean blackPawnsBlocked = !canExecuteMove(Player.BLACK);

    // check whether player white is still able to make a move
    boolean whitePawnsBlocked = !canExecuteMove(Player.WHITE);

    if (blackPawnsBlocked && whitePawnsBlocked) {
      // Both players have all of their pawns blocked and are not able to make a move anymore.
      state.setCurrentPhase(Phase.FINISHED);
      if (whitePawns.size() > blackPawns.size()) {
        state.setWinner(Optional.of(Player.WHITE));
      } else if (blackPawns.size() > whitePawns.size()) {
        state.setWinner(Optional.of(Player.BLACK));
      } else {
        assert blackPawns.size() == whitePawns.size();
        state.setWinner(Optional.empty());
      }
      return true;
    }

    return false;
  }

  /**
   * Checks a player if he is able to execute a move with at least one of the pawns.
   *
   * @param player The player to be checked.
   * @return <code>true</code> if there is at least one pawn that is able to move, <code>false
   * </code> otherwise.
   */
  public boolean canExecuteMove(Player player) {
    Set<Cell> cells = state.getAllCellsOfPlayer(player);
    for (Cell cell : cells) {
      Set<Cell> possibleMoves = getPossibleMovesForPawn(player, cell);
      if (!possibleMoves.isEmpty()) {
        // There is at least one pawn available for the player that is able to move
        return true;
      }
    }
    return false;
  }

  @Override
  public Set<Cell> getPossibleMovesForPawn(Cell cell) {
    return getPossibleMovesForPawn(state.getCurrentPlayer(), cell);
  }

  /**
   * Computes all possible moves for a current selected pawn. There are four possible moves in total
   * available for a single pawn, depending on the current position of the pawn as well as the
   * position of pawns from the opponent player.
   *
   * @param currentPlayer The current {@link Player player}.
   * @param cell          The {@link Cell cell} that the {@link Pawn pawn} is currently positioned.
   * @return A set of cells with all possible moves for the current selected pawn.
   */
  private Set<Cell> getPossibleMovesForPawn(Player currentPlayer, Cell cell) {
    if (currentPlayer != Player.WHITE && currentPlayer != Player.BLACK) {
      throw new IllegalArgumentException("Unhandled player: " + currentPlayer);
    }
    if (!state.getField().isCellOfPlayer(currentPlayer, cell)) {
      throw new IllegalArgumentException(
              "Cell " + cell + " does not belong to player " + currentPlayer);
    }

    Set<Cell> possibleMoves = new HashSet<>();

    GameField field = state.getField();
    int direction = currentPlayer == Player.WHITE ? DIRECTION_UPWARD : DIRECTION_DOWNWARD;

    int sourceCol = cell.getColumn();
    int sourceRow = cell.getRow();

    // check the immediate two upper (lower) neighbors of the same column
    Cell oneAhead = new Cell(sourceCol, sourceRow + direction);
    if (field.isWithinBounds(oneAhead) && field.get(oneAhead).isEmpty()) {
      possibleMoves.add(oneAhead);

      Cell twoAhead = new Cell(sourceCol, sourceRow + 2 * direction);
      if (field.isWithinBounds(twoAhead)
              && field.get(twoAhead).isEmpty()
              && sourceRow == getBaseRowIndexOfPlayer(currentPlayer)) {
        possibleMoves.add(twoAhead);
      }
    }

    Player opponent = Player.getOpponentOf(currentPlayer);

    // Add the forward left point if it is currently owned by the opponent player
    Cell diagonallyForwardLeft = new Cell(sourceCol - direction, sourceRow + direction);
    if (state.getField().isWithinBounds(diagonallyForwardLeft)
            && field.isCellOfPlayer(opponent, diagonallyForwardLeft)) {
      possibleMoves.add(diagonallyForwardLeft);
    }

    // Add the forward right point if it is currently owned by the opponent player
    Cell diagonallyForwardRight = new Cell(sourceCol + direction, sourceRow + direction);
    if (state.getField().isWithinBounds(diagonallyForwardRight)
            && field.isCellOfPlayer(opponent, diagonallyForwardRight)) {
      possibleMoves.add(diagonallyForwardRight);
    }

    return possibleMoves;
  }

  public void setArtIntelligent(boolean artIntelligent) {
    this.artIntelligent = artIntelligent;
  }

  public boolean getArtIntelligent() {
    return this.artIntelligent;
  }

  public double getGameScore() {
    return this.gameScore;
  }

  /**
   * creating children from a node.
   * @param stateTree is the parent node of the childrens.
   * @throws CloneNotSupportedException avoid exception because deep copy is performed here.
   */
  public void createChildren(ModelNode stateTree) throws CloneNotSupportedException {
    Model model = stateTree.getModel();
    ArrayList<Cell> fromCells = getMovablePawn(model);

    for (Cell from : fromCells) {
      Set<Cell> toCellsSet = model.getPossibleMovesForPawn(from);
      ArrayList<Cell> toCells = new ArrayList<>(toCellsSet);
      if (model.getState().getCurrentPlayer() == Player.BLACK) {
        toCells.sort(Comparator.reverseOrder());
      } else {
        toCells.sort(Comparator.naturalOrder());
      }
      for (Cell toCell : toCells) {
        Model tempModel = new Chess(model);
        tempModel.move(from,toCell);
        ModelNode child = new ModelNode(tempModel,from,toCell);
        stateTree.addChild(child);
        int parentHeight = child.getParent().getHeight();
        child.setHeight(parentHeight + 1);
      }
    }
    //if (this.think) {
    //  for (int i = 0; i < stateTree.getChildren().size();i++) {
    //    System.out.println(stateTree.getChildren().get(i).getFrom() + " " + i);
    //  }
    //System.out.println(" ");
    //}
  }

  private ArrayList<Cell> getMovablePawn(Model model) {
    ArrayList<Cell> fromCells = new ArrayList<>();
    Map<Cell,Player> occupiedCells = model.getState().getField().getCellsOccupiedWithPawns();
    Map<Cell, Player> cellOccupiedWithCurrentPawns = occupiedCells.entrySet()
            .stream().filter(map -> model.getState().getCurrentPlayer().equals(map.getValue()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    for (Map.Entry<Cell,Player> cell : cellOccupiedWithCurrentPawns.entrySet()) {
      Cell fromCell = cell.getKey();
      fromCells.add(fromCell);
    }
    if (model.getState().getCurrentPlayer() == Player.BLACK) {
      fromCells.sort(Comparator.reverseOrder());
    } else {
      fromCells.sort(Comparator.naturalOrder());
    }
    return fromCells;
  }
}
