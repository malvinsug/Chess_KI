package chess.model;

import java.util.ArrayList;
import java.util.Map;

import java.util.stream.Collectors;

/**
 * This class implements the score of each game state in a model class.
 * The calculation is based on the given lecture.
 */
public class ScoringState {
  // This shows all the score of the very first state.
  private static double N_SCORE = -4;

  private static double D_SCORE = 0;

  private static double C_SCORE = 0;
  private static double ch = 0;
  private static double cm = 0;

  private static double I_SCORE = 0;
  private static double ih = 0;
  private static double im = 0;

  private static double V_SCORE = 0;
  private static double vh = 0;
  private static double vm = 0;

  private static double gameScore;

  /**
   * Update the game score.
   */
  private static void updateGameScore() {
    gameScore = N_SCORE + D_SCORE + C_SCORE + I_SCORE + V_SCORE;
  }

  /**
   * Returns the initial game score.
   * @return the intial score of the chess game.
   */
  public static double initialScore() {
    updateGameScore();
    return gameScore;
  }

  /**
   * It updates all N, D, C, I, V scores and sums them all up.
   * @param model is the current chess game.
   * @param from is the previous cell of a selected pawn before it's moved.
   * @param to is the new cell of a selected pawn after it's moved.
   * @return the actual game score.
   */
  public static double updateAll(Model model,Cell from, Cell to) {
    updateN(model);
    updateD(model);
    updateC(model);
    updateI(model,from,to);
    updateV(model,from,to);
    updateGameScore();

    //debugging();

    return gameScore;
  }

  private static void debugging() {
    System.out.println("N = " + N_SCORE + "," + " D = " + D_SCORE + "," + " C = " + C_SCORE + ","
            + " I = " + I_SCORE + "," + " V = " + V_SCORE + " = " + gameScore);
  }

  /**
   * Updates the N score.
   * @param model is the current model.
   */
  private static void updateN(Model model) {
    double whitePawns = 0;
    double blackPawns = 0;
    Map<Cell, Player> cellWithPawns = model.getState().getField().getCellsOccupiedWithPawns();
    for (Map.Entry<Cell, Player> element : cellWithPawns.entrySet()) {
      if (element.getValue() == Player.BLACK) {
        blackPawns++;
      } else if (element.getValue() == Player.WHITE) {
        whitePawns++;
      }
    }
    N_SCORE = blackPawns - 1.5 * whitePawns;
  }

  /**
   * Updates the D score.
   * @param model is the current chess game.
   */
  private static void updateD(Model model) {
    int fromRow;
    D_SCORE = 0;
    Map<Cell, Player> cellWithPawns = model.getState().getField().getCellsOccupiedWithPawns();
    for (Map.Entry<Cell, Player> element : cellWithPawns.entrySet()) {
      if (element.getValue() == Player.BLACK) {
        fromRow = 7 - element.getKey().getRow();
        D_SCORE += fromRow;
      } else if (element.getValue() == Player.WHITE) {
        fromRow = element.getKey().getRow();
        D_SCORE = D_SCORE - 1.5 * fromRow;
      }
    }
  }

  /**
   * update the C score.
   * @param model is the current chess game.
   */
  private static void updateC(Model model) {
    ch = 0;
    cm = 0;
    Player currentPlayer = model.getState().getCurrentPlayer();
    Map<Cell, Player> cellOccupiedWithPawns = model.getState()
            .getField().getCellsOccupiedWithPawns();

    if (currentPlayer == Player.WHITE) {
      //System.out.println("white");
      Map<Cell, Player> cellOccupiedWithBlackPawns = cellOccupiedWithPawns.entrySet()
              .stream().filter(map -> Player.BLACK.equals(map.getValue()))
              .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

      for (Map.Entry<Cell, Player> blackPawn : cellOccupiedWithBlackPawns.entrySet()) {
        Cell checkedPawn = blackPawn.getKey();

        int checkedRow = checkedPawn.getRow();
        int checkColumn = checkedPawn.getColumn();

        //+ because protecting pawn should be below the black pawn.
        int friend1Row = checkedRow + 1;
        int friend1Column = checkColumn - 1;
        int friend2Row = checkedRow + 1;
        int friend2Column = checkColumn + 1;

        int enemy1Row = checkedRow - 1;
        int enemy1Column = checkColumn - 1;
        int enemy2Row = checkedRow - 1;
        int enemy2Column = checkColumn + 1;

        if ((pawnNotProtected(model,friend1Column, friend1Row, friend2Column, friend2Row))
                && canBeCaptured(model,enemy1Column, enemy1Row, enemy2Column, enemy2Row)) {
          cm += 1;
        }
      }
    } else {
      //System.out.println("black");
      Map<Cell, Player> cellOccupiedWithWhitePawns = cellOccupiedWithPawns.entrySet()
              .stream().filter(map -> Player.WHITE.equals(map.getValue()))
              .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

      for (Map.Entry<Cell,Player> whitePawn : cellOccupiedWithWhitePawns.entrySet()) {
        Cell checkedPawn = whitePawn.getKey();

        int checkedRow = checkedPawn.getRow();
        int checkColumn = checkedPawn.getColumn();

        int friend1Row = checkedRow - 1;
        int friend1Column = checkColumn - 1;
        int friend2Row = checkedRow - 1;
        int friend2Column = checkColumn + 1;

        int enemy1Row = checkedRow + 1;
        int enemy1Column = checkColumn - 1;
        int enemy2Row = checkedRow + 1;
        int enemy2Column = checkColumn + 1;

        if ((pawnNotProtected(model,friend1Column, friend1Row, friend2Column, friend2Row))
                && canBeCaptured(model,enemy1Column, enemy1Row, enemy2Column, enemy2Row)) {
          ch += 1;
        }
      }
    }
    C_SCORE = ch - 1.5 * cm;
  }

  /**
   * Checked whether the given pawn can be captured or not.
   * @param model is the chess model.
   * @param e1Column is the column number of the first enemy cell.
   * @param e1Row is the row number of the first enemy cell.
   * @param e2Column is the column number of the second enemy cell.
   * @param e2Row is the row number of the second enemy cell.
   * @return returns true if the pawn can be captured, else false.
   */
  private static boolean canBeCaptured(Model model, int e1Column, int e1Row,
                                       int e2Column, int e2Row) {
    boolean e1Exist = isEExist(model, e1Column, e1Row);
    boolean e2Exist = isEExist(model, e2Column, e2Row);
    return (e1Exist || e2Exist);
  }

  /**
   * Check whether the given pawn is protected or not.
   * @param model is the chess model.
   * @param f1Column is the column cell number of the first pawn protecting this pawn.
   * @param f1Row is the row cell number of the first pawn protecting this pawn.
   * @param f2Column is the column cell number of the second pawn protecting this pawn.
   * @param f2Row is the row cell number of the second pawn protecting this pawn.
   * @return if there is another pawn protected this pawn then it returns true, else false.
   */
  private static boolean pawnNotProtected(Model model,int f1Column, int f1Row,
                                       int f2Column, int f2Row) {
    boolean f1Exist = isFExist(model,f1Column, f1Row);
    boolean f2Exist = isFExist(model,f2Column, f2Row);
    return !(f1Exist || f2Exist);
  }

  /**
   * Checked whether the protecting pawn exist or not.
   * @param model is the chess model.
   * @param f1Column is the column number of the protecting cell.
   * @param f1Row is the row number of the protecting cell.
   * @return if the protecting pawn exists, then it returns true, otherwise false.
   */
  private static boolean isFExist(Model model, int f1Column, int f1Row) {
    Cell f1;
    if (f1Column >= 0 && f1Column <= 7 && f1Row >= 0 && f1Row <= 7) {
      f1 = new Cell(f1Column, f1Row);

      return model.getState().getField().get(f1).isPresent()
              && model.getState().getField().get(f1)
              .get().getPlayer() != model.getState().getCurrentPlayer();
    }
    return false;
  }

  /**
   * Checked whether the pawns that are able to capturing the moved pawn exist or not.
   * @param model is the chess model.
   * @param e1Column is the column number of the enemy cell.
   * @param e1Row is the row number of the enemy cell.
   * @return if the enemy pawn exists, then it returns true, otherwise false.
   */
  private static boolean isEExist(Model model,int e1Column, int e1Row) {
    Cell e1;
    if (e1Column >= 0 && e1Column <= 7 && e1Row >= 0 && e1Row <= 7) {
      e1 = new Cell(e1Column, e1Row);
      return model.getState().getField().get(e1).isPresent()
              && model.getState().getField().get(e1)
              .get().getPlayer() == model.getState().getCurrentPlayer();
    }
    return false;
  }

  /**
   * Updates the I score.
   * @param model is the current chess game.
   * @param from is the previous cell of a selected pawn before it's moved.
   * @param to is the new cell of a selected pawn after it's moved.
   */
  private static void updateI(Model model,Cell from, Cell to) {
    ih = 0;
    im = 0;
    Map<Cell, Player> cellOccupiedWithPawns = model.getState()
            .getField().getCellsOccupiedWithPawns();

    for (Map.Entry<Cell,Player> pawn : cellOccupiedWithPawns.entrySet()) {
      Cell checkedPawnCell = pawn.getKey();
      Player checkedPawnPlayer = pawn.getValue();

      int centerColumn = checkedPawnCell.getColumn();
      int centerRow = checkedPawnCell.getRow();

      ArrayList<Cell> environment = setEnvironment(centerColumn, centerRow);

      boolean friendExist = false;
      for (Cell candidate : environment) {
        if (model.getState().getField().get(candidate).isPresent()
                && model.getState().getField().get(candidate)
                .get().getPlayer() == checkedPawnPlayer) {
          friendExist = true;
          break;
        }
      }
      if (!friendExist) {
        updateIhIm(checkedPawnPlayer);
      }
    }
    I_SCORE = ih - 1.5 * im;
  }

  /**
   * Setting the possible cells around the checked pawn.
   * @param centerColumn the column number of the checked pawn.
   * @param centerRow the row number of the checked pawn.
   * @return an array of possible cells around the checked pawn.
   */
  private static ArrayList<Cell> setEnvironment(int centerColumn, int centerRow) {
    ArrayList<Cell> environment = new ArrayList<>();
    int leftCol = centerColumn - 1;
    int rightCol = centerColumn + 1;

    int upRow = centerRow - 1;

    if (cellExists(rightCol)) {
      environment.add(new Cell(rightCol,centerRow));
    }
    if (cellExists(leftCol)) {
      environment.add(new Cell(leftCol,centerRow));
    }

    if (cellExists(upRow)) {
      environment.add(new Cell(centerColumn,upRow));
    }

    int downRow = centerRow + 1;

    if (cellExists(downRow)) {
      environment.add(new Cell(centerColumn, downRow));
    }

    if (cellExists(rightCol) && cellExists(upRow)) {
      environment.add(new Cell(rightCol,upRow));
    }

    if (cellExists(rightCol) && cellExists(downRow)) {
      environment.add(new Cell(rightCol,downRow));
    }

    if (cellExists(leftCol) && cellExists(upRow)) {
      environment.add(new Cell(leftCol,upRow));
    }

    if (cellExists(leftCol) && cellExists(downRow)) {
      environment.add(new Cell(leftCol, downRow));
    }
    return environment;
  }

  /**
   * Checking whether the possible cell is out of bound or not.
   * @param position column number or row number.
   * @return if it's out of bound, then return false, otherwise true.
   */
  private static boolean cellExists(int position) {
    return (position >= 0 && position <= 7);
  }

  /**
   * update the ih or im score.
   * @param currentPlayer the
   */
  private static void updateIhIm(Player currentPlayer) {
    if (currentPlayer == Player.WHITE) {
      ih += 1;
    } else if (currentPlayer == Player.BLACK) {
      im += 1;
    }
  }

  /**
   * It updates the V score.
   * @param model is the current chess game.
   * @param from is the previous cell of a selected pawn before it's moved.
   * @param to is the new cell of a selected pawn after it's moved.
   */
  private static void updateV(Model model,Cell from,Cell to) {
    if (false/*in future this steps leads to win*/) {
      int steps = 1;
      vm = 5000 / steps;
    } else {
      vm = 0;
      vh = 0;
    }

  }
}
