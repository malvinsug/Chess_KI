package chess.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of a tree class for supporting the Minimax algorithm.
 * Each node contents a chess model, a pawn move and also the
 * best or worst next node for AI player.
 */
public class ModelNode {
  private Model model;

  private Cell from;
  private Cell to;
  private double score;

  private ModelNode favouriteChild = null;
  private ModelNode parent = null;

  private List<ModelNode> children = new ArrayList<>();
  private int height;

  /**
   * Initialize a new node. It's not a tree.
   * @param model shows the state of the game.
   * @param from is the previous cell of last selected move.
   * @param to is the new cell for last selected move.
   */
  public ModelNode(Model model, Cell from, Cell to) {
    this.model = model;
    this.from = from;
    this.to = to;
    this.score = model.getGameScore();
    this.height = 0;
  }

  public Model getModel() {
    return this.model;
  }

  public void setModel(Model model) {
    this.model = model;
  }

  public Cell getFrom() {
    return this.from;
  }

  public Cell getTo() {
    return this.to;
  }

  public double getScore() {
    return this.score;
  }

  public void setScore(double score) {
    this.score = score;
  }

  /**
   * A method for getting best or worst node for AI player.
   * @return is a best or worst next node for AI player.
   */
  public ModelNode getFavouriteChild() {
    return this.favouriteChild;
  }

  /**
   * A method for setting the best or the worst next node for AI player.
   * @param favouriteChild is the best or the worst next node.
   */
  public void setFavouriteChild(ModelNode favouriteChild) {
    this.favouriteChild = favouriteChild;
  }

  /**
   * Adding a new child to this node. If this is
   * used, than it will make a tree.
   * @param child is a new node.
   */
  public void addChild(ModelNode child) {
    child.setParent(this);
    this.children.add(child);
  }

  /**
   * A method for getting all possible next nodes.
   * @return all possible next nodes.
   */
  public List<ModelNode> getChildren() {
    return this.children;
  }

  public ModelNode getParent() {
    return this.parent;
  }

  /**
   * A method for setting a parent for this node. (It sets the
   * previous move.)
   * @param parent is the previous node.
   */
  public void setParent(ModelNode parent) {
    this.parent = parent;
  }

  public int getHeight() {
    return this.height;
  }

  public void setHeight(int height) {
    this.height = height;
  }
}