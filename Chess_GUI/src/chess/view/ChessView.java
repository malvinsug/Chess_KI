package chess.view;

import static java.util.Objects.requireNonNull;

import chess.model.Model;
import chess.model.Phase;
import chess.model.Player;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.util.Optional;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * The main view of the graphical user interface. It provides and connects all graphical elements
 * that are necessary for playing a chess game. More specifically, it allows two players to
 * interactively execute their moves, and provides an adequate possibility for resetting the game at
 * any given time.
 */
public class ChessView extends JFrame implements View {

  private static final long serialVersionUID = 4230162211355981591L;

  private static final int MINIMUM_FRAME_WIDTH = 400;
  private static final int MINIMUM_FRAME_HEIGHT = 400;

  private final DrawBoard drawBoard;

  private Model model;
  private Controller controller;

  private JPanel resetPanel;
  private JButton resetButton;
  private JLabel infoLabel;
  private JLabel errorLabel;

  /**
   * Creates a view with all elements necessary for playing an interactive chess game.
   *
   * @param model The {@link Model} that handles the logic of the game.
   * @param controller The {@link Controller} that validates and forwards any user input.
   */
  public ChessView(Model model, Controller controller) {
    super("Pawn Game");

    this.model = requireNonNull(model);
    this.controller = requireNonNull(controller);

    drawBoard = new DrawBoard(model, controller);

    setMinimumSize(new Dimension(MINIMUM_FRAME_WIDTH, MINIMUM_FRAME_HEIGHT));
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    initializeWidgets();
    createView();
  }

  private void initializeWidgets() {
    int fontsize = 14;

    infoLabel = new JLabel();
    infoLabel.setForeground(Color.RED);
    infoLabel.setFont(new Font(infoLabel.getFont().getFontName(), Font.PLAIN, fontsize));

    resetPanel = new JPanel(new FlowLayout());

    resetButton = new JButton("Reset");
    resetButton.addActionListener(new ChessViewListener());
    resetButton.setEnabled(true);
    errorLabel = new JLabel();
    errorLabel.setForeground(Color.RED);
    errorLabel.setFont(new Font(errorLabel.getFont().getFontName(), Font.PLAIN, fontsize));
  }

  private void createView() {
    JPanel panel = new JPanel(new FlowLayout());
    panel.add(infoLabel);
    add(panel, BorderLayout.NORTH);

    add(drawBoard, BorderLayout.CENTER);

    resetPanel.add(resetButton);
    resetPanel.add(errorLabel);
    add(resetPanel, BorderLayout.SOUTH);
  }

  @Override
  public void showGame() {
    showMenu();
    updatePlayerTextInDisplay();
    setSize(800, 800);
    setVisible(true);
  }

  private void showMenu() {
    String[] options = {"Hotseat","Single"};
    int gamePlay = JOptionPane.showOptionDialog(null,
            "Which game you want to play?",
            "Pawn Game",
            JOptionPane.PLAIN_MESSAGE,
            JOptionPane.QUESTION_MESSAGE,
            null,
            options,
            options);
    if (gamePlay == 0) {
      model.setArtIntelligent(false);
    } else {
      model.setArtIntelligent(true);
    }
  }

  @Override
  public void showErrorMessage(String message) {
    errorLabel.setText(message);
  }

  /** Hides an error message that was previously displayed to the user. */
  private void hideErrorMessage() {
    errorLabel.setText("");
  }

  /** Updates the label text that informs the user about the current player. */
  private void updatePlayerTextInDisplay() {
    setInfoLabelText("Current player: " + model.getState().getCurrentPlayer());
  }

  @Override
  public void dispose() {
    drawBoard.dispose();
    controller.dispose();
    super.dispose();
  }

  @Override
  public void propertyChange(PropertyChangeEvent event) {
    SwingUtilities.invokeLater(
        new Runnable() {
          @Override
          public void run() {
            handleChangeEvent(event);
          }
        });
  }

  /**
   * The observable (= model) has just published that it has changed its state. The GUI needs to be
   * updated accordingly here.
   *
   * @param event The event that has been fired by the model.
   */
  private void handleChangeEvent(PropertyChangeEvent event) {
    if (event.getPropertyName().equals(Model.STATE_CHANGED)) {
      updatePlayerTextInDisplay();
      openDialogIfGameIsOver();
    }
    hideErrorMessage();
  }

  /**
   * Checks the model if the game has ended. In that case, a dialog is shown to the user in which a
   * respective message with the winner is shown.
   */
  private void openDialogIfGameIsOver() {
    if (model.getState().getCurrentPhase() != Phase.FINISHED) {
      return;
    }

    Optional<Player> playerOpt = model.getState().getWinner();
    if (playerOpt.isPresent()) {
      Player p = playerOpt.get();
      setInfoLabelText("Player " + p + " has won. Click reset to start a new game.");
      showDialogWindow("Finished!", "There's a winner: Player " + p);
    } else {
      setInfoLabelText("Game ended as a tie. Click reset to start a new game.");
      showDialogWindow("Finished!", "Game is over. It's a tie!");
    }
  }

  /**
   * Shows a message pane that displays the outcome of the game.
   *
   * @param header The header text.
   * @param message An elaborate message why the game has ended.
   */
  private void showDialogWindow(String header, String message) {
    Optional<ImageIcon> iconOpt = ResourceLoader.loadScaledImageIcon("images/award.png", 60, 80);
    JOptionPane.showMessageDialog(
            this,
            message,
            header,
            JOptionPane.INFORMATION_MESSAGE,
            iconOpt.isPresent() ? iconOpt.get() : null);
  }

  /**
   * Display a message in the view.
   *
   * @param message The message to be displayed
   */
  private void setInfoLabelText(String message) {
    infoLabel.setText(message);
  }

  /**
   * A custom implementation of an {@link ActionListener} that is used for observing and forwarding
   * any actions made on the respective widgets of the {@link ChessView}.
   */
  private class ChessViewListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      if (e.getSource() == resetButton) {
        handleResetButtonClick();
      }
    }

    private void handleResetButtonClick() {
      int result =
              JOptionPane.showConfirmDialog(
                      ChessView.this,
                      "Do you really want to start a new game?",
                      "Please confirm your choice",
                      JOptionPane.YES_NO_OPTION,
                      JOptionPane.QUESTION_MESSAGE);
      showMenu();
      // do nothing if user didn't click on the 'yes'-option
      if (result == JOptionPane.YES_OPTION) {
        controller.resetGame();
      }
    }
  }
}