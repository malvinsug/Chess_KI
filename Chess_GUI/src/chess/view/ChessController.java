package chess.view;

import static java.util.Objects.requireNonNull;

import chess.model.Cell;
import chess.model.Model;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;



/**
 * Implementation of the controller interface which handles the interaction between the main model-
 * and view-classes. Its main purpose is to check the validity of all incoming requests and to take
 * the appropriate actions afterwards.
 */
public class ChessController implements Controller {

  private Model model;
  private View view;

  private SwingWorker<Boolean, Void> moveWorker;

  /**
   * Creates a controller object for a given model.
   *
   * @param chess A non-null {@link Model model-object} that handles the logic of the game.
   */
  public ChessController(Model chess) {
    model = requireNonNull(chess);
  }

  @Override
  public void setView(View view) {
    this.view = requireNonNull(view);
  }

  @Override
  public void start() {
    view.showGame();
  }

  @Override
  public void resetGame() {
    model.newGame();
  }

  @Override
  public boolean move(Cell from, Cell to) {
    if (moveWorker != null) {
      showError("Move is already in progress");
      return false;
    }
    if (!model.getState().getField().isWithinBounds(from)) {
      showError("Source cell is out of bounds: " + from);
      return false;
    }
    if (!model.getState().getField().isWithinBounds(to)) {
      showError("Target cell is out of bounds: " + to);
      return false;
    }

    // SwingWorker is designed to be executed only once. Executing it more often will
    // not result in invoking the doInBackground method for another time.
    // Due to this, we are using null references here in order to check if there are
    // any pending moves.
    moveWorker =
        new SwingWorker<>() {

          @Override
          protected Boolean doInBackground() throws Exception {
            return model.move(from, to);
          }

          @Override
          protected void done() {
            moveWorker = null;
            try {
              if (!get().booleanValue()) {
                showError("Failed to execute the move");
              }
            } catch (InterruptedException e) {
              // We should not have to wait in the first place
              throw new IllegalStateException(e.getMessage(), e);
            } catch (ExecutionException e) {
              throw new RuntimeException(e.getMessage(), e);
            } catch (CancellationException e) {
              // moveWorker got interrupted, probably intentionally by the user.
              // --> do nothing here
            }
          }
        };
    moveWorker.execute();

    return true;
  }

  private void showError(String message) {
    view.showErrorMessage(message);
  }

  @Override
  public void dispose() {
    if (moveWorker != null) {
      // cancel the pending move
      moveWorker.cancel(true);
    }
    model.removePropertyChangeListener(view);
  }
}
